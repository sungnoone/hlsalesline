from socketIO_client import SocketIO, BaseNamespace
from datetime import datetime as dt

SOCKETIO_SERVER_HOST = "192.168.1.227"
SOCKETIO_SERVER_PORT = 5000

## Socket Namespace define
class Chat_Namespace(BaseNamespace):
    def on_my_response(self, *args):

        print('on_my_response', args)

## Sending message to center
def send_message_to_center(from_user_id, from_user_name, to_user_id, to_user_name,message_text):
    socketIO = SocketIO(SOCKETIO_SERVER_HOST, SOCKETIO_SERVER_PORT)
    chat1_namespace = socketIO.define(Chat_Namespace, "/chat1")
    now_datetime = dt.now().strftime("%Y-%m-%d %H:%M:%S")
    msg_json = {"From_Line_User_ID":from_user_id,
                "From_Line_User_Name":from_user_name,
                "To_Line_User_ID": to_user_id,
                "To_Line_User_Name":to_user_name,
                "Message_Content":message_text,
                "Rec_DateTime":now_datetime,
                "Have_Read":"false"}
    chat1_namespace.emit('received', msg_json)
    socketIO.wait(seconds=1)
