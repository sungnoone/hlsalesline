## 2018/08/07
## Azure VM :  hlsalesline
## Linebot API Service

from flask import Flask, request, abort, render_template, send_file
import requests
from socketIO_client import SocketIO
import json
import os

##custom function
from func.mysocketio import send_message_to_center

from linebot import (
    LineBotApi, WebhookHandler
)

from linebot.exceptions import (
    InvalidSignatureError, LineBotApiError
)

from linebot.models import *

app = Flask(__name__)

line_bot_api = LineBotApi("tczTFP7XNwr9nBBLqXt1N2xxk3vYSG973VgAJBLe5qlRIkZhkhELtA0UdvzmUziTKG/dpPaTUDA+FkozNJZLs8XTmSS8GEvaJjvJ9MIfm1WQpeby4H4CUk/xV7poc+sxNgrl0JcLwiEupmPXMKu6JgdB04t89/1O/w1cDnyilFU=")
handler = WebhookHandler("58fd9e4fe24432d41313bcb943cb8470")
LINE_BOT_ID = '@sfl7430m'

##Line Bot API 網址
LINEBOTAPI_URL = "https://56757aa9.ngrok.io"

##配合Line Bot API 提供服務範例網址
#HL_LINE_API_URL = "https://316614df.ngrok.io"
HL_LINE_API_HOST = "http://192.168.1.238:3388"

##綁定身份
HLLINEWEB_BIND_URL = HL_LINE_API_HOST + "/db/bind/lineid"

##==================Line Bot API 功能==================

##Line@ Web URL 欄位測通用
@app.route('/callback', methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    app.logger.info("Request body:" + body)
    ##handle webhook body
    try:
        handler.handle(body,signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

##攔截文字物件訊息
def num_to_string(num):
    numbers = {
        0: "個人專區",
        1: "樣書申請",
        2: "意見反映"
    }
    return numbers.get(num)

@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    ##get source user id
    source_line_user_id = event.source.user_id
    ## get user profile
    try:
        profile = line_bot_api.get_profile(source_line_user_id)
        print(profile.display_name)
    except LineBotApiError as e:
        # error handle
        print("Get Line User Profile Error:")
        print(e)
    ##文字訊息分眾
    if event.message.text == num_to_string(0):
        carousel_template_message = TemplateSendMessage(
            alt_text='個人專區',
            template=CarouselTemplate(
                columns=[
                    CarouselColumn(
                        thumbnail_image_url=LINEBOTAPI_URL+"/pics?name=group.png",
                        title="個人專區",
                        text="個人專區",
                        actions=[
                            #PostbackAction(
                            #    label="身分查詢",
                            #    text=source_line_user_id,
                            #    data="action="+source_line_user_id
                            #),
                            MessageAction(
                                label="我的ID",
                                text=source_line_user_id
                            ),
                            URIAction(
                                label="綁定身分",
                                uri= HLLINEWEB_BIND_URL+"?lineuserid="+source_line_user_id
                            )
                        ]
                    )
                ]
            )
        )
        line_bot_api.reply_message(
            event.reply_token,
            carousel_template_message
        )
    elif event.message.text == num_to_string(1):
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=num_to_string(1)+"中...")
        )
    elif event.message.text == num_to_string(2):
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=num_to_string(2)+"中...")
        )
    else:
        ## send message to message_center
        send_message_to_center(from_user_id=source_line_user_id, from_user_name=profile.display_name, to_user_id=LINE_BOT_ID, to_user_name="翰貓", message_text=event.message.text)
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=source_line_user_id+" 說： "+event.message.text)
        )

##攔截貼圖物件訊息
@handler.add(MessageEvent, message=StickerMessage)
def handle_message_sticker(event):
    line_bot_api.reply_message(
        event.reply_token,
        StickerSendMessage(package_id=1,sticker_id=1)
    )

##攔截檔案物件訊息
@handler.add(MessageEvent, message=FileMessage)
def handle_message_file(event):
    file_name = event.message.file_name
    file_type = event.message.type
    file_id = event.message.id
    file_size = event.message.file_size
    message_content = line_bot_api.get_message_content(str(file_id))
    local_file_path = os.path.abspath("Y:/"+file_name)
    with open(local_file_path, 'wb') as fd:
        for chunk in message_content.iter_content():
            fd.write(chunk)
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage("已收到檔案:")
    )

##==================網頁功能==================

@app.route('/index')
def hello():
    return 'Hello, Python!!'


##身分綁定
#@app.route("/register")
#def user_identification():
#    return render_template("")

##圖片網址
@app.route("/pics")
def pics_url():
    pic_name = request.args.get("name")
    print(pic_name)
    return send_file("./pics/"+pic_name, mimetype="image/png")

##==================中介API==================
##圖片網址
@app.route("/line/push", methods=["GET","POST"])
def line_batch_push_message():
    if request.method=="GET":
        return "Batch sending line message api."
    else:
        dataJSON = json.loads(request.data)
        print(type(dataJSON))
        for elem in dataJSON:
            ##push message
            line_bot_api.push_message(elem, TextSendMessage(text=dataJSON[elem]))
            print(dataJSON[elem])
        return "POST"


if __name__ == '__main__':
    app.run()
